(require 'package)
(add-to-list 'package-archives
             '("melpa" . "http://melpa.org/packages/") t)
(package-initialize) ;; 加载 melpa 的源
(add-to-list 'auto-mode-alist (cons "\\.adoc\\'" 'adoc-mode)) ;; 自动启动adoc模式当打开 *.adoc 文件时
(add-to-list 'auto-mode-alist (cons "\\.asciidoc\\'" 'adoc-mode)) ;; 自动启动adoc模式当打开 *.asciidoc 文件时
(projectile-global-mode)
(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(custom-enabled-themes (quote (zen-and-art)))
 '(custom-safe-themes
   (quote
    ("4eaad15465961fd26ef9eef3bee2f630a71d8a4b5b0a588dc851135302f69b16" default))))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 )
(setq make-backup-files nil);; 设定不产生备份文件
(setq-default indent-tabs-mode nil);; not use tab  
(custom-set-variables '(coffee-tab-width 2));; coffee tab 缩进 2 空格
(setq
   make-backup-files nil
   backup-by-copying t      ; don't clobber symlinks
   backup-directory-alist
    '(("." . "~/.saves"))    ; don't litter my fs tree
   delete-old-versions t
   kept-new-versions 6
   kept-old-versions 2
   version-control t)       ; use versioned backups
(setq
  scss-compile-at-save nil) ; not auto compile scss files
