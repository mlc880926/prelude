;;; adoc-mode-autoloads.el --- automatically extracted autoloads
;;
;;; Code:


;;;### (autoloads (adoc-mode) "adoc-mode" "adoc-mode.el" (21696 47468
;;;;;;  271135 372000))
;;; Generated autoloads from adoc-mode.el

(autoload 'adoc-mode "adoc-mode" "\
Major mode for editing AsciiDoc text files.
Turning on Adoc mode runs the normal hook `adoc-mode-hook'.

\(fn)" t nil)

;;;***

;;;### (autoloads nil nil ("adoc-mode-pkg.el") (21696 47468 294036
;;;;;;  203000))

;;;***

(provide 'adoc-mode-autoloads)
;; Local Variables:
;; version-control: never
;; no-byte-compile: t
;; no-update-autoloads: t
;; coding: utf-8
;; End:
;;; adoc-mode-autoloads.el ends here
