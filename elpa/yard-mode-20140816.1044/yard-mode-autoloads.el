;;; yard-mode-autoloads.el --- automatically extracted autoloads
;;
;;; Code:
(add-to-list 'load-path (or (file-name-directory #$) (car load-path)))

;;;### (autoloads nil "yard-mode" "yard-mode.el" (21689 51193 410775
;;;;;;  602000))
;;; Generated autoloads from yard-mode.el

(autoload 'yard-mode "yard-mode" "\
Font locking and completion for YARD tags and directives

\(fn &optional ARG)" t nil)

;;;***

;; Local Variables:
;; version-control: never
;; no-byte-compile: t
;; no-update-autoloads: t
;; End:
;;; yard-mode-autoloads.el ends here
