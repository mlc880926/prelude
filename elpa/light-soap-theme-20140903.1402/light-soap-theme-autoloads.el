;;; light-soap-theme-autoloads.el --- automatically extracted autoloads
;;
;;; Code:


;;;### (autoloads nil "light-soap-theme" "light-soap-theme.el" (21696
;;;;;;  47795 779133 464000))
;;; Generated autoloads from light-soap-theme.el

(when load-file-name (add-to-list 'custom-theme-load-path (file-name-as-directory (file-name-directory load-file-name))))

;;;***

;;;### (autoloads nil nil ("light-soap-theme-pkg.el") (21696 47795
;;;;;;  809222 21000))

;;;***

(provide 'light-soap-theme-autoloads)
;; Local Variables:
;; version-control: never
;; no-byte-compile: t
;; no-update-autoloads: t
;; coding: utf-8
;; End:
;;; light-soap-theme-autoloads.el ends here
